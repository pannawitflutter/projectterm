import 'package:basic_recipes_project/Screen/SettingScreen.dart';
import 'package:flutter/foundation.dart';
import 'package:basic_recipes_project/Screen/SearchScreen.dart';
import 'package:basic_recipes_project/constants.dart';
import 'package:basic_recipes_project/Screen/recipe_detail.dart';
import 'package:basic_recipes_project/setfunction.dart';
import 'package:flutter/material.dart';
import 'package:basic_recipes_project/data.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';
import 'package:basic_recipes_project/RecipeList.dart';
import 'package:like_button/like_button.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[50],
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      top: 0.00, bottom: 20.00, left: 20.0, right: 20.00),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Welcome",
                                  style: TextStyle(
                                    fontSize: 28.00,
                                    fontWeight: FontWeight.w800,
                                    color: Colors.red,
                                  ),
                                ),
                                Text(
                                  "พร้อมสำหรับอาหารของคุณแล้วรึยัง ?",
                                  style: TextStyle(
                                    fontSize: 14.00,
                                    fontWeight: FontWeight.w800,
                                    color: Colors.red,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {},
                            child: Container(
                              decoration: BoxDecoration(shape: BoxShape.circle),
                              padding: EdgeInsets.only(
                                left: 6.0,
                                bottom: 6.0,
                                right: 6.0,
                                top: 6.0,
                              ),
                              margin: EdgeInsets.only(
                                left: 20.00,
                                bottom: 10.00,
                              ),
                              child: Image.asset(
                                "images/logo_basicRecipes.png",
                                width: 100.00,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 50),
                      RecipeList()
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
  // This trailing comma makes auto-formatting nicer for build methods.

}
