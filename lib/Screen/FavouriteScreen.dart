import 'package:basic_recipes_project/RecipeList.dart';
import 'package:basic_recipes_project/Screen/recipe_detail.dart';
import 'package:flutter/material.dart';
import 'package:basic_recipes_project/data.dart';
import 'package:basic_recipes_project/gridList.dart';

class favouriteScreen extends StatefulWidget {
  @override
  _favouriteScreenState createState() => _favouriteScreenState();
}

class _favouriteScreenState extends State<favouriteScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Center(
            child: Text(
          "Favourite List",
          style: TextStyle(color: Colors.black),
        )),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: 700,
          child: GridList(),
        ),
      ),
    );
  }
}
