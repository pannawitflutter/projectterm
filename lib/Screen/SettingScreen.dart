import 'package:flutter/material.dart';
import 'package:basic_recipes_project/constants.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:basic_recipes_project/Screen/HomeScreen.dart';
import 'package:basic_recipes_project/Screen/SearchScreen.dart';
import 'package:basic_recipes_project/ProfileMenu.dart';

class SettingScreen extends StatefulWidget {
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<SettingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: Center(
              child: Text(
            "Profile",
            style: TextStyle(color: Colors.black),
          )),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: Column(
            children: [
              SizedBox(
                height: 115,
                width: 115,
                child: Stack(
                  fit: StackFit.expand,
                  clipBehavior: Clip.none,
                  children: [
                    CircleAvatar(
                      backgroundImage: AssetImage("images/Jamie-Social.jpg"),
                    ),
                    Positioned(
                      right: -16,
                      bottom: 0,
                      child: SizedBox(
                        height: 46,
                        width: 46,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50),
                              side: BorderSide(color: Colors.white),
                            ),
                            primary: Colors.white,
                            backgroundColor: Colors.grey,
                          ),
                          onPressed: () {},
                          child: Icon(Icons.add_a_photo),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Container(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
                  child: TextButton(
                    style: TextButton.styleFrom(primary: Colors.grey[70]),
                    onPressed: () {},
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.person_outline_rounded, color: Colors.red),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                            child: Text(
                          "My Account",
                          style: TextStyle(color: Colors.red),
                        )),
                        Icon(Icons.arrow_forward_ios, color: Colors.red)
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
                child: TextButton(
                  style: TextButton.styleFrom(primary: Colors.grey[70]),
                  onPressed: () {},
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.notifications_active_outlined,
                          color: Colors.red),
                      SizedBox(
                        width: 20,
                      ),
                      Expanded(
                          child: Text("Notification",
                              style: TextStyle(color: Colors.red))),
                      Icon(Icons.arrow_forward_ios, color: Colors.red)
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
                child: TextButton(
                  style: TextButton.styleFrom(primary: Colors.grey[70]),
                  onPressed: () {},
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.settings_outlined, color: Colors.red),
                      SizedBox(
                        width: 20,
                      ),
                      Expanded(
                          child: Text("Setting",
                              style: TextStyle(color: Colors.red))),
                      Icon(Icons.arrow_forward_ios, color: Colors.red)
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
                child: TextButton(
                  style: TextButton.styleFrom(primary: Colors.grey[70]),
                  onPressed: () {},
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.logout_outlined, color: Colors.red),
                      SizedBox(
                        width: 20,
                      ),
                      Expanded(
                          child: Text("Logout",
                              style: TextStyle(color: Colors.red))),
                      Icon(Icons.arrow_forward_ios, color: Colors.red)
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
