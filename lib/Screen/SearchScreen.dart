import 'dart:ui';
import 'package:basic_recipes_project/RecipeListRow.dart';
import 'package:basic_recipes_project/Screen/HomeScreen.dart';
import 'package:basic_recipes_project/Screen/SettingScreen.dart';
import 'package:basic_recipes_project/constants.dart';
import 'package:flutter/material.dart';
import 'package:basic_recipes_project/RecipeList.dart';
import 'package:basic_recipes_project/data.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => new _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(height: 250.0, color: Colors.red),
              Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(15.0, 35.0, 15.0, 10.0),
                    child: Material(
                      elevation: 10.0,
                      borderRadius: BorderRadius.circular(25.0),
                      child: TextFormField(
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            prefixIcon: Icon(Icons.search, color: Colors.black),
                            contentPadding:
                                EdgeInsets.only(left: 15.0, top: 15.0),
                            hintText: 'Search for recipes and channels',
                            hintStyle: TextStyle(color: Colors.grey)),
                      ),
                    ),
                  ),
                  SizedBox(height: 15.0),
                  Padding(
                    padding: EdgeInsets.only(left: 15.0),
                    child: Container(
                      padding: EdgeInsets.only(left: 10.0),
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Colors.orange,
                                  style: BorderStyle.solid,
                                  width: 3.0))),
                      child: Row(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('การค้นหา',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20.0,
                                      fontFamily: 'Timesroman',
                                      fontWeight: FontWeight.bold)),
                              Text('ยอดนิยม',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20.0,
                                      fontFamily: 'Timesroman',
                                      fontWeight: FontWeight.bold)),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 15.0),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 15.0, left: 15.0),
                    height: 125.0,
                    child: RecipeListRow(),
                  )
                ],
              )
            ],
          ),
          SizedBox(height: 15.0),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 15.0),
            child: Text(
              'รายการ',
              style: TextStyle(
                  fontFamily: 'Quicksand', color: Colors.grey, fontSize: 14.0),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 15.0),
            child: Text(
              'การค้นหา',
              style: TextStyle(
                  fontFamily: 'Timesroman',
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0),
            ),
          ),
          SizedBox(height: 10.0),
          RecipeList(),
        ],
      ),
    ));
  }
//   Widget _foodCard() {
//   return Container(
//     height: 125.0,
//     width: 250.0,
//     decoration: BoxDecoration(
//       borderRadius: BorderRadius.circular(12.0),
//       color: Colors.white,
//     ),
//     child: Row(
//       children: <Widget>[
//         GestureDetector(),
//         Container(
//           decoration: BoxDecoration(
//               borderRadius: BorderRadius.circular(12.0),
//               image: DecorationImage(image: AssetImage(rec))),
//           height: 125.0,
//           width: 100.0,
//         ),
//         SizedBox(width: 20.0),
//       ],
//     ),
//   );
// }
}

class DataSearch extends SearchDelegate<String> {
  final keyword = ['ส้มตำ', 'ก๋วยเตี๋ยว', 'ต้มยำ', 'ไก่ทอด'];
  final recentKeyword = ['ส้มตำ', 'ก๋วยเตี๋ยว', 'ต้มยำ'];

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = "";
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, "");
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    final suggestionList;
    return Center(
        child: Container(
            height: 100,
            width: 100,
            child: Card(
              color: Colors.red,
              child: Text(
                query,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            )));
    print(query);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty
        ? recentKeyword
        : keyword.where((p) => p.startsWith(query)).toList();

    return ListView.builder(
        itemBuilder: (context, index) => ListTile(
              onTap: () {
                if (recentKeyword.contains(suggestionList[index])) {
                  print(suggestionList[index]);
                  String textSearch = suggestionList[index];
                  showResults(context);
                } else {
                  recentKeyword.add(suggestionList[index]);
                }
              },
              leading: Icon(Icons.menu_book),
              title: RichText(
                text: TextSpan(
                    text: suggestionList[index].substring(0, query.length),
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                    children: [
                      TextSpan(
                        text: suggestionList[index].substring(query.length),
                        style: TextStyle(color: Colors.grey),
                      )
                    ]),
              ),
            ),
        itemCount: suggestionList.length);
  }
}
