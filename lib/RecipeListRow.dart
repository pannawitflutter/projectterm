import 'package:flutter/material.dart';
import 'package:basic_recipes_project/Screen/recipe_detail.dart';
import 'package:basic_recipes_project/data.dart';
import 'package:like_button/like_button.dart';

class RecipeListRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: Recipes.demoRecipe.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
            child: GestureDetector(
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          RecipesDetail(recipes: Recipes.demoRecipe[index]))),
              child: RecipeCardRow(
                recipe: Recipes.demoRecipe[index],
              ),
            ),
          );
        });
  }
}

class RecipeCardRow extends StatefulWidget {
  final Recipes recipe;
  RecipeCardRow({
    required this.recipe,
  });

  @override
  _RecipeCardRowState createState() => _RecipeCardRowState();
}

class _RecipeCardRowState extends State<RecipeCardRow> {
  bool loved = false;
  bool saved = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10),
      height: 125.0,
      width: 250.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        color: Colors.white,
      ),
      child: Row(children: <Widget>[
        Container(
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(image: AssetImage(widget.recipe.imgPath))),
          height: 75.0,
          width: 75.0,
        ),
        SizedBox(width: 20.0),
        Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Text(
            widget.recipe.title,
            style:
                TextStyle(fontFamily: 'Quicksand', fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 10.0),
          Container(
            height: 2.0,
            width: 75.0,
            color: Colors.orange,
          ),
          SizedBox(height: 10.0),
        ]),
      ]),
    );
  }
}
