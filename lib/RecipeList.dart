import 'package:flutter/material.dart';
import 'package:basic_recipes_project/Screen/recipe_detail.dart';
import 'package:basic_recipes_project/data.dart';
import 'package:like_button/like_button.dart';

class RecipeList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: ScrollPhysics(),
      shrinkWrap: true,
      itemCount: Recipes.demoRecipe.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 12,
            vertical: 12,
          ),
          child: InkWell(
            onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => RecipesDetail(
                    recipes: Recipes.demoRecipe[index],
                  ),
                )),
            child: RecipeCard(
              recipe: Recipes.demoRecipe[index],
            ),
          ),
        );
      },
    );
  }
}

class RecipeCard extends StatefulWidget {
  final Recipes recipe;
  RecipeCard({
    required this.recipe,
  });

  @override
  _RecipeCardState createState() => _RecipeCardState();
}

class _RecipeCardState extends State<RecipeCard> {
  bool loved = false;
  bool saved = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(24),
                child: Hero(
                  tag: widget.recipe.imgPath,
                  child: Image(
                    height: 320,
                    width: 320,
                    fit: BoxFit.cover,
                    image: AssetImage(widget.recipe.imgPath),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 20,
              right: 40,
              child: InkWell(
                onTap: () {
                  setState(() {
                    saved = !saved;
                  });
                },
              ),
            ),
          ],
        ),
        SizedBox(
          height: 0,
        ),
        ClipRRect(
            borderRadius:
                BorderRadius.vertical(bottom: const Radius.circular(30)),
            child: Container(
              color: Colors.white,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 24.0, vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.recipe.title,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Text(
                            widget.recipe.writer,
                            style: Theme.of(context).textTheme.caption,
                          ),
                        ],
                      ),
                    ),
                    // Spacer(),
                    Flexible(
                      flex: 1,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          SizedBox(
                            width: 20,
                          ),
                          Icon(
                            Icons.timer,
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Text(
                            widget.recipe.cookingTime.toString() + '\'',
                          ),
                          Spacer(),
                          InkWell(
                              onTap: () {
                                setState(() {
                                  loved = !loved;
                                });
                              },
                              child: LikeButton()),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ))
      ],
    );
  }
}
