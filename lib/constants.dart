import 'package:flutter/material.dart';

Color kPrimaryColor = Color(0xFF27AE60);

BoxShadow kBoxShadow = BoxShadow(
  color: Colors.grey.withOpacity(0.2),
  spreadRadius: 2,
  blurRadius: 8,
  offset: Offset(0, 0),
);

const ColorWhite = Color(0xFFF3F3F3);
const ColorWhiteGrey = Color(0xFFC6C5C5);
const kSpacingUnit = 10;
