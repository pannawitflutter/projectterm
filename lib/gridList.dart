import 'dart:math';

import 'package:basic_recipes_project/constants.dart';
import 'package:flutter/material.dart';
import 'package:basic_recipes_project/Screen/recipe_detail.dart';
import 'package:basic_recipes_project/data.dart';
import 'package:flutter/services.dart';
import 'package:like_button/like_button.dart';

class GridList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 75,
        crossAxisSpacing: 20,
      ),
      physics: ScrollPhysics(),
      shrinkWrap: true,
      itemCount: Recipes.demoRecipe.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 10,
          ),
          child: GestureDetector(
            onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => RecipesDetail(
                    recipes: Recipes.demoRecipe[index],
                  ),
                )),
            child: GridFav(
              recipe: Recipes.demoRecipe[index],
            ),
          ),
        );
      },
    );
  }
}

class GridFav extends StatefulWidget {
  final Recipes recipe;
  GridFav({
    required this.recipe,
  });

  @override
  _GridFavState createState() => _GridFavState();
}

class _GridFavState extends State<GridFav> {
  bool loved = false;
  bool saved = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Stack(
          alignment: Alignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 10.00),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(32.00),
                boxShadow: [kBoxShadow],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    child: Image(
                        image: AssetImage(widget.recipe.imgPath),
                        fit: BoxFit.fitWidth),
                    padding: EdgeInsets.only(bottom: 5.00),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 12.00,
                    ),
                    child: Text(
                      widget.recipe.title,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.00,
                        fontWeight: FontWeight.w600,
                        color: Colors.red,
                      ),
                    ),
                  ),
                  SizedBox(height: 12.00),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.timer),
                      Padding(padding: EdgeInsets.only(left: 5, right: 5)),
                      Text(
                        widget.recipe.cookingTime.toString() + " Min",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 12.00,
                          fontWeight: FontWeight.w600,
                          color: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ],
    );
  }
}
